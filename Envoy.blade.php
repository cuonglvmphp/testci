@servers(['staging' => ['ec2-user@3.138.111.72']])

@setup
    $dir = "/var/testci";
@endsetup

@story('deploy')
    git
    build
@endstory

@task('git', ['on' => $server])
    cd {{ $dir }}
    sudo su
    pwd
    git checkout master
    git pull origin master
@endtask

@task('build', ['on' => $server])
    cd {{ $dir }}
    pwd
    docker exec -it nginx /bin/sh
    cd /var/www/html
    composer install
    php artisan migrate --force
    php artisan cache:clear
    php artisan config:clear
    npm install
    npm run {{ $npm }}
@endtask
